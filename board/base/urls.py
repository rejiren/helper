from django.conf.urls import patterns, include, url

from board.base.views import *

urlpatterns = patterns('',
    url(r'^old$', home, name='home1'),
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^section/(?P<id>\d+)/$', SectionView.as_view(), name='section'),
    url(r'^post/(?P<id>\d+)/$', PostView.as_view(), name='post'),
)
