from django import forms
from .models import Post, Section

class SectionForm(forms.ModelForm):
    class Meta:
        model = Section