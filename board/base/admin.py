from django.contrib import admin
from .models import Section, Post, File

admin.site.register(Post)
admin.site.register(File)
admin.site.register(Section)