from django.db import models


class Section(models.Model):
    short_title = models.CharField(max_length=20)
    title = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    parent = models.ForeignKey(
        'self',
        blank=True,
        null=True,
    )

    def __unicode__(self):
        return self.short_title

    @property
    def posts(self):
        return Post.objects.filter(section=self)


class File(models.Model):
    name = models.CharField(max_length=255)
    file = models.FileField(upload_to='materials')

    def __unicode__(self):
        return self.name

class Post(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    img = models.ImageField(upload_to="post_img", null=True, blank=True)
    small_img = models.ImageField(upload_to="post_small_img", null=True, blank=True)

    is_carousel = models.BooleanField(default=False)
    on_main = models.BooleanField(default=False)

    section = models.ForeignKey(Section)
    files = models.ManyToManyField(File)

    def __unicode__(self):
        return self.title

    def materials(self):
        return self.files.all()