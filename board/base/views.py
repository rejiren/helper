from django.shortcuts import render
from .models import Section

def home(request):
    return render(request, 'front/base.html', locals())

from django.views.generic import TemplateView, DetailView, FormView
from .forms import SectionForm
from .models import Section, Post


class SectionViewMixin(object):

    def get_current_sections(self):
        return None, None

    @property
    def sections(self):
        current_section, current_sub_section = self.get_current_sections()

        sections = Section.objects.filter(parent=None).values('id', 'short_title', 'parent')
        sub_sections = Section.objects.filter(
            parent=current_section
        ).values(
            'id',
            'short_title',
            'parent'
        ) if current_section else []

        return {
            'top_sections': sections,
            'sub_sections': sub_sections,
            'current_top_section': current_section,
            'current_sub_section': current_sub_section,
        }

    def get_context_data(self, **kwargs):
        context = super(SectionViewMixin, self).get_context_data(**kwargs)
        context['sections'] = self.sections
        return context


class HomeView(SectionViewMixin, TemplateView):
    template_name = 'front/home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['slider_posts'] = Post.objects.filter(is_carousel=True)
        context['on_main_posts'] = Post.objects.filter(on_main=True)
        return context


class SectionView(SectionViewMixin, DetailView):
    model = Section
    pk_url_kwarg = 'id'
    template_name = 'front/section.html'

    def get_current_sections(self):
        if self.object.parent:
            return self.object.parent.id, self.object.id
        else:
            return self.object.id, None


class PostView(SectionViewMixin, DetailView):
    model = Post
    pk_url_kwarg = 'id'
    template_name = 'front/post.html'

    def get_current_sections(self):
        section = self.object.section
        if section.parent:
            return section.parent.id, section.id
        else:
            return section.id, None


#class FormView(FormView):
#    form_class = SectionForm
#    template_name = 'front/base.html'