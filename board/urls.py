from django.conf.urls import patterns, include, url
from django.contrib import admin
from board import settings

admin.autodiscover()


urlpatterns = patterns('',
    url(r'^', include('board.base.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#urlpatterns += patterns('',
#    url(r'static/(?P<path>.*)$', 'django.views.static.serve',
#        {'document_root': settings.STATIC_ROOT}),
#)